package takshak.mace.takshak2k18;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ScannerActivity extends AppCompatActivity {
    private static final String MYPREF = "mypreferenece";
    private static final String EMAIL = "email";
    private CodeScanner mCodeScanner;
    Button sendbutton;
    EditText qrcodedittext;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        sendbutton=findViewById(R.id.sendbutton);
        qrcodedittext = findViewById(R.id.uidtextview);
        sharedPreferences=getSharedPreferences(MYPREF,MODE_PRIVATE);
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                ScannerActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ScannerActivity.this, result.getText(), Toast.LENGTH_SHORT).show();
                        qrcodedittext.setText(result.getText());
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCodeScanner.startPreview();
            }
        });
        mCodeScanner.startPreview();

        sendbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (qrcodedittext.getText().toString().length()<=0){
                    Toast.makeText(getApplicationContext(),"NO UID",Toast.LENGTH_LONG).show();
                    return;
                }
                else {
                    AlertDialog alertDialog;
                    AlertDialog.Builder builder = new AlertDialog.Builder(ScannerActivity.this,R.style.MyDialogTheme);
                    builder.setTitle("Confirm").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            new AttendanceAsyncTask().execute();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

    }
    class AttendanceAsyncTask extends AsyncTask<String ,Void ,String>{

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            String qrcode=qrcodedittext.getText().toString();
            String email=sharedPreferences.getString(EMAIL,"noemail");
            email = email.replace("@","%40");
            String urlf = "http://demo1275613.mockable.io/attendance?email="+email+"&qrcode="+qrcode;
            Log.d("TAG",urlf);
            Request request = new Request.Builder()
                    .url(urlf)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                Log.d("response","ok");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sendbutton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            sendbutton.setEnabled(true);
            if (s==null){
                Toast.makeText(getApplicationContext(),"ERROR",Toast.LENGTH_LONG).show();
                return;
            }else {
                try {
                    Log.d("TAG",s);
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status==200){
                        String name = jsonObject.getString("name");
                        String mobile = jsonObject.getString("mobile");
                        Toast.makeText(getApplicationContext(),"Name : "+name+"\nmobile : "+mobile,Toast.LENGTH_LONG).show();
                        qrcodedittext.setText("");
                    }else {
                        Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
